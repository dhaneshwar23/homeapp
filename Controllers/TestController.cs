﻿using Microsoft.AspNetCore.Mvc;

namespace HomeApp.Controllers
{
    public class TestController : Controller
    {
        [HttpGet("api/user")]
        public IActionResult Get()
        {
            return Ok(new {name = "22 Ridware Crescent"});
        }
    }
}
