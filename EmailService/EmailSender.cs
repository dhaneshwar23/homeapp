﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;


namespace EmailService
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailConfiguration _emailConfig;
        private readonly ILogger<EmailSender> _logger;

        public EmailSender(EmailConfiguration emailConfig,ILogger<EmailSender> logger)
        {
            _emailConfig = emailConfig;
            _logger = logger;
        }

        /*public void SendEmail(Message message)
        {
            var emailMessage = CreateEmailMessage(message);

            Send(emailMessage);
        }

        public async Task SendEmailAsync(Message message)
        {
            var mailMessage = CreateEmailMessage(message);

            await SendAsync(mailMessage);
        }*/

        
        public async Task SendEmails(string email,string firstName,string lastName,string callbackUrl,string subject, string actionName)
        {
            string body = string.Empty;
            
            if (actionName =="CreateUser")
                body = "<p>Hi {0} {1},</p><p>Welcome to Home Task App.</p><p>Please click on below link to activate your account.</p><p>{2}</p><p>Thanks,</p><p>Home APP Team</p><p>*******Please do not reply to this email. This is auto generated email.*******&quot;</p>";
            else if (actionName == "ForgotPassword")
                body = "<p>Hi {0} {1},</p><p>Welcome to Home Task App.</p><p>Please click on below link to reset your login password.</p><p>{2}</p><p>Thanks,</p><p>Home APP Team</p><p>*******Please do not reply to this email. This is auto generated email.*******</p>";

            var message = new MailMessage();
            message.To.Add(new MailAddress(email));
            message.From = new MailAddress("hometaskv1@gmail.com");
            message.Subject = subject;
            message.Body = string.Format(body, firstName, lastName, callbackUrl);
            message.IsBodyHtml = true;

            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "hometaskv1@gmail.com",  // replace with valid value
                    Password = ",E]UFy=Un}LC3yx%"  // replace with valid value
                };
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }
        }

        public async Task SendtaskEmail(string email, string firstName, string lastname, string subject, string actionname)
        {
            string body = string.Empty;

            if (actionname == "SendTaskEmail")

            body = "<p>Hi {0} ,</p><p> Your Todays task is to Clean the Kitchen</p><p>Task Desctiption:Please Clean Stove, Counter Top, Sweep and Mop the Floor as well</p><p>Thanks,</p><p>Home APP Team</p><p>*******Please do not reply to this email. This is auto generated email.*******&quot;</p>";
            else if (actionname == "ForgotPassword")
                body = "<p>Hi {0} ,</p><p>Welcome to Home Task App.</p><p>Please click on below link to reset your login password.</p><p>{2}</p><p>Thanks,</p><p>Home APP Team</p><p>*******Please do not reply to this email. This is auto generated email.*******</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress(email));
            message.From = new MailAddress("hometaskv1@gmail.com");
            message.Subject = subject;
            message.Body = string.Format(body, firstName/*, lastname*/);
            message.IsBodyHtml = true;

            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "hometaskv1@gmail.com",  // replace with valid value
                    Password = ",E]UFy=Un}LC3yx%"  // replace with valid value

                };
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = credential;
                _logger.LogInformation("Credential issues for email ");
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                //smtp.Port = 465;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
                _logger.LogInformation("Email sent Maybe?");
                
            }


        }

        public async Task SendTaskEmailToAdmin(string email,string adminFirstName, string firstName, string subject, string actionname, string messages)
        {
            string body = string.Empty;

            if (actionname == "SendTaskEmail")

                body = "<p>Hi {0} ,</p><p> {1}'s Todays task is to Clean the Kitchen</p><p>Task Desctiption:Please Clean Stove, Counter Top, Sweep and Mop the Floor as well</p><p>Thanks,</p><p>Home APP Team</p><p>*******Please do not reply to this email. This is auto generated email.*******&quot;</p>";
            else if (actionname == "ForgotPassword")
                body = "<p>Hi {0} ,</p><p>Welcome to Home Task App.</p><p>Please click on below link to reset your login password.</p><p>{2}</p><p>Thanks,</p><p>Home APP Team</p><p>*******Please do not reply to this email. This is auto generated email.*******</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress(email));
            message.From = new MailAddress("hometaskv1@gmail.com");
            message.Subject = subject;
            message.Body = string.Format(body, adminFirstName,firstName/*, lastname*/, messages);
            message.IsBodyHtml = true;

            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "hometaskv1@gmail.com",  // replace with valid value
                    Password = ",E]UFy=Un}LC3yx%"  // replace with valid value

                };
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = credential;
                _logger.LogInformation("Credential issues for email ");
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                //smtp.Port = 465;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
                _logger.LogInformation("Email sent Maybe?");

            }

        }

        /*private MimeMessage CreateEmailMessage(Message message)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_emailConfig.From));
            emailMessage.To.AddRange(message.To);
            emailMessage.Subject = message.Subject;

            var bodyBuilder = new BodyBuilder { HtmlBody = string.Format("<h2 style='color:red;'>{0}</h2>", message.Content) };

            if (message.Attachments != null && message.Attachments.Any())
            {
                byte[] fileBytes;
                foreach (var attachment in message.Attachments)
                {
                    using (var ms = new MemoryStream())
                    {
                        attachment.CopyTo(ms);
                        fileBytes = ms.ToArray();
                    }

                    bodyBuilder.Attachments.Add(attachment.FileName, fileBytes, ContentType.Parse(attachment.ContentType));
                }
            }

            emailMessage.Body = bodyBuilder.ToMessageBody();
            return emailMessage;
        }*/

        /*private void Send(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect(_emailConfig.SmtpServer, _emailConfig.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_emailConfig.UserName, _emailConfig.Password);

                    client.Send(mailMessage);
                }
                catch
                {
                    //log an error message or throw an exception, or both.
                    throw;
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
        }*/

        private async Task SendAsync(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(_emailConfig.SmtpServer, _emailConfig.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(_emailConfig.UserName, _emailConfig.Password);

                    await client.SendAsync(mailMessage);
                }
                catch
                {
                    //log an error message or throw an exception, or both.
                    throw;
                }
                finally
                {
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
        }
    }
}
