﻿using System;

namespace EmailService
{
    public class EmailConfiguration
    {
        public String From { get; set; }
        public String SmtpServer { get; set; }
        public int Port { get; set; }
        public String UserName { get; set; }
        public String Password { get; set; }
    }
}
