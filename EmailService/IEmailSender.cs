﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailService
{
    public interface IEmailSender
    {
        /*void SendEmail(Message message);

        Task SendEmailAsync(Message message);*/

        Task SendEmails(string email, string FirstName, string lastName, string callbackUrl, string subject, string actionName);

        Task SendtaskEmail(string email, string FirstName, string lastname, string subject, string actionname);

        Task SendTaskEmailToAdmin(string email, string adminFirstName, string FirstName, string subject, string actionname,string messages);
    }
}
