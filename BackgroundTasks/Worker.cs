using EmailService;
using HomeApp.Controllers;
using HomeApp.Data;
using HomeApp.Model;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BackgroundTasks
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly UserDbContext _userDbContext;
        private readonly IEmailSender _emailService;
        private CrontabSchedule _schedule;
        private DateTime _nextRun;

        private string Schedule => "*/10 * * * * *"; //Runs every 10 seconds

        private readonly GenerateTask _generateTask;

        public Worker(ILogger<Worker> logger, UserDbContext userDbContext,GenerateTask generateTask, IEmailSender emailSender)
        {
            _emailService = emailSender;
            _generateTask = generateTask;
            _userDbContext = userDbContext;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                List<KitchenTask> tasks = _generateTask.GetKitchenTask();

                if (tasks != null)
                {
                    SendEmail(tasks);

                }
                await Task.Delay(1000, stoppingToken);

            }
        }

        private void SendEmail(List<KitchenTask> tasks)
        {
            foreach (var t in tasks)
            {
                while (t.task_id == 1)
                {
                    _emailService.SendtaskEmail(t.Email, t.FirstName, t.LastName, "Please Complete your Daily Task", "SendTaskEmail");
                }
            }

            

        }
    }
}
