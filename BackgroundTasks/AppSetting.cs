﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackgroundTasks
{
    public static class AppSetting
    {
        public static IConfiguration Configuration { get; set; }
    }
}
