﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class ChangePassword
    {
        
        [Key]
        public string email { get; set; }

        [Required(ErrorMessage = "Current Password is required")]
        public string currpwd { get; set; }

        [Required(ErrorMessage = "New Password is required")]
        public string newpwd { get; set; }

        /*[Required(ErrorMessage = "Confirm Password is required")]
        public string ConfirmPassword { get; set; }*/
    }
}
