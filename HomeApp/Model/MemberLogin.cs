﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class MemberLogin
    {

        [Key]
        public int Member_Id { get; set; }

        public string Username { get; set; }
        

        /*[EmailAddress]*/
        public string Email { get; set; }

        public string Password { get; set; }

    }
}
