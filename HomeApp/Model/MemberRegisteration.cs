﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class MemberRegisteration
    {
        [Key]
        public int Member_Id { get; set; }

        public string Username { get; set; }

       public string Firstname { get; set; }

       public string Lastname { get; set; }

       
       public String Birthdate { get; set; }

       [DataType(DataType.PhoneNumber)]
       public string PhoneNumber { get; set; }

        /*[EmailAddress]
        [Required]*/

       public string Email { get; set; }

        /*[Required]*/
       public string Password { get; set; }

        public string CurrentPassword { get; set; }

        public string Role { get; set; }

        public string UserId { get; set; }

        public string CreatedBy { get; set; }

     
        

        


    }
}
