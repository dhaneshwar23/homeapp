﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class ApplicationUserBackUp
    {
        [Key]
        public string Id { get; set; }
        public string DisplayName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [DataType(DataType.Date)]
        public string BirthDay { get; set; }
        public string ProfilePic { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        [Required]
        public DateTime? Created { get; set; }

        public DateTime? LastUpdated { get; set; }
        public string LastUpdateBy { get; set; }


        public string Username { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }


        public String Birthdate { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        /*[EmailAddress]
        [Required]*/

        public string Email { get; set; }

        /*[Required]*/
        public string Password { get; set; }

        public string CurrentPassword { get; set; }

        public string Role { get; set; }

        public string UserId { get; set; }

        


    }
}
