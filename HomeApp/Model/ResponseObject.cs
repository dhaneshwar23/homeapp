﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class ResponseObject
    {
        public bool IsValid { get; set; }

        public string Message { get; set; }

        public string Data { get; set; }
    }
}
