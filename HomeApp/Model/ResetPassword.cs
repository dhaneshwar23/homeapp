﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class ResetPassword
    {
        public string Password { get; set; }

        public string UserId { get; set; }

        public string Code { get; set; }
    }
}
