﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class ShowTaskList
    {
        public int Task_id { get; set; }
        public string TaskName { get; set; }
    }
}
