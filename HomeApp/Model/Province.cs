﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class Province
    {
        [Key]
        public int? province_id { get; set; }

       /* public int? Country_Id { get; set; }*/

        public string province_code { get; set; }

        public string province_name { get; set; }

        
    }
}
