﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class ApplicationUser : IdentityUser
    {
        /*public string UserId { get; set; }*/
        public string DisplayName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [DataType(DataType.Date)]
        public string BirthDay { get; set; }
        public string ProfilePic { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        [Required]
        public DateTime? Created { get; set; }
        
        public DateTime? LastUpdated { get; set; }
        public string LastUpdateBy { get; set; }

       


       




    }
}
