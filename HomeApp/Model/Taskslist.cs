﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class Taskslist
    {   [Key]
        public int Task_id { get; set; }
        public string TaskName { get; set; }
    }
}
