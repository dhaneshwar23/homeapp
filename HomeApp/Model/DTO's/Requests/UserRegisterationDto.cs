﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model.DTO_s.Requests
{
    public class UserRegisterationDto
    {
        [Required]
        public string username;

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }
    }
}
