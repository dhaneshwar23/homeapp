﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class HomeRegisterations
    {
        [Key]
        public int Home_id { get; set; }

        public string Home_Name { get; set; }

        public int Street_no { get; set; }

        public string Street_Name { get; set; }

        public string Country { get; set; }

        public string province { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }
    }
}
