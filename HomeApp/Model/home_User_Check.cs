﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class home_User_Check
    {   [Key]
        public int homeuser_id { get; set; }

        public string Id { get; set; }

        public string home_id { get; set; }
    }
}
