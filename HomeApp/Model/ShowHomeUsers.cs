﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class ShowHomeUsers
    {
        [Key]
        public string Id { get; set; }

        public int Home_id { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String home_name { get; set; }
    }
}
