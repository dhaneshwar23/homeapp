﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class WeeklyTaskDescription
    {
        [Key]
        public int Weekly_Task_Id { get; set; }

        public string Task_Description { get; set; }
    }
}
