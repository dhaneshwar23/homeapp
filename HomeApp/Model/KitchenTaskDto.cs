﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Model
{
    public class KitchenTaskDto
    {
        [Key]
        public int task_id { get; set; }

        

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string email { get; set; }
        
    }
}
