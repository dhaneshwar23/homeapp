﻿using EmailService;
using HomeApp.Configuration;
using HomeApp.Data;
using HomeApp.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;

namespace HomeApp.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly UserDbContext _userDbContext;

        private readonly ApplicationSettings _applicationSettings;

        private readonly IEmailSender _emailSender;
        private readonly JwtConfig _jwtConfig;
        private readonly ILogger<UserService> _logger;
        private IHttpContextAccessor _httpContextAccessor;

        public UserService(UserManager<ApplicationUser> userManager, 
            UserDbContext userDbContext,SignInManager<ApplicationUser> signInManager,
            IOptions<ApplicationSettings> appSettings,
            IEmailSender emailSender, 
            IOptionsMonitor<JwtConfig> optionsMonitor,
            ILogger<UserService> logger,
            IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _userManager = userManager;
            _userDbContext = userDbContext;
            _signInManager = signInManager;
            _applicationSettings = appSettings.Value;
            _emailSender = emailSender;
            _jwtConfig = optionsMonitor.CurrentValue;
            _httpContextAccessor = httpContextAccessor;

        }

        

        public async Task<IdentityResult> RegisterAdminAsync(MemberRegisteration memberRegisteration)
        {
            var user = new ApplicationUser
            {
                Email = memberRegisteration.Email,
                UserName = memberRegisteration.Email,
                FirstName = memberRegisteration.Firstname,
                lastName = memberRegisteration.Lastname,
                BirthDay = memberRegisteration.Birthdate.ToString(),
                PhoneNumber = memberRegisteration.PhoneNumber,
                Created = DateTime.UtcNow,
                CreatedBy = memberRegisteration.Firstname


            };
            var userResult = await _userManager.CreateAsync(user, memberRegisteration.Password);
            if (userResult.Succeeded)
            {
                var roleResult = await _userManager.AddToRoleAsync(user, memberRegisteration.Role);
                if (roleResult.Succeeded)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                    ApplicationSettings appSettings = new ApplicationSettings();
                    var url = appSettings.GetConfigurationValue("ApplicationURL", "EmailConfirmationURL");

                    var callbackUrl = url + "?userId=" + user.Id + "&code=" + HttpUtility.UrlEncode(code);

                    await _emailSender.SendEmails(memberRegisteration.Email, memberRegisteration.Firstname, memberRegisteration.Lastname, HtmlEncoder.Default.Encode(callbackUrl), "Home Task Account activation link", "CreateUser");

                    var homeuser = new home_User_Check
                    {
                        Id = user.Id,
                        home_id = null

                    };
                    _userDbContext.homeusercheck.Add(homeuser);
                    _userDbContext.SaveChanges();
                }


            }


            return userResult;
        }

        public async Task<IdentityResult> RegisterUserAsync(MemberRegisteration memberRegisteration)
        {
            
            var user = new ApplicationUser
            {
                Email = memberRegisteration.Email,
                UserName = memberRegisteration.Email,
                FirstName = memberRegisteration.Firstname,
                lastName = memberRegisteration.Lastname,
                BirthDay = memberRegisteration.Birthdate.ToString(),
                PhoneNumber = memberRegisteration.PhoneNumber,
                Created = DateTime.UtcNow,
                CreatedBy = memberRegisteration.CreatedBy
            
                
                    
                };
                var userResult = await _userManager.CreateAsync(user, memberRegisteration.Password);
            

            
            if (userResult.Succeeded)
            {
                var roleResult = await _userManager.AddToRoleAsync(user, memberRegisteration.Role);
                if (roleResult.Succeeded)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                    ApplicationSettings appSettings = new ApplicationSettings();
                    var url = appSettings.GetConfigurationValue("ApplicationURL", "EmailConfirmationURL");

                    var callbackUrl = url + "?userId=" + user.Id + "&code=" + HttpUtility.UrlEncode(code);

                    await _emailSender.SendEmails(memberRegisteration.Email, memberRegisteration.Firstname, memberRegisteration.Lastname, HtmlEncoder.Default.Encode(callbackUrl), "Home Task Account activation link", "CreateUser");
                }

                var homeuser = new home_User_Check { 
                Id=user.Id,
                home_id=null
                
                };
                _userDbContext.homeusercheck.Add(homeuser);
                _userDbContext.SaveChanges();
            }
            return userResult;
        }

        

        public async Task<LoginResult> SignInAsync(MemberLogin model)
        {
            var loginResult = new LoginResult();
            var user = await _userManager.FindByNameAsync(model.Email);

            if (user != null)
            {
                if (user.EmailConfirmed == false)
                {
                    loginResult.Success = false;
                    loginResult.Message = "Your account is not activated, Please confirm account activation link in your registered email.";
                    return loginResult;
                }
                
            }

            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true, lockoutOnFailure: false);
            /*var result = await _userManager.CheckPasswordAsync(user, model.Password);*/

            if (result.Succeeded)
            {
                if (result.IsLockedOut)
                {
                    loginResult.Success = false;
                    loginResult.Message = "Your account is locked, Please contact your administrator to unlock your account.";
                    return loginResult;

                }
                var role = await _userManager.GetRolesAsync(user);

                ApplicationSettings appSettings = new ApplicationSettings();
                var secretKey = appSettings.GetConfigurationValue("JWT", "JWT_Secret");
                IdentityOptions _options = new IdentityOptions();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID",user.Id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType,role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddMinutes(30),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey)), SecurityAlgorithms.HmacSha256Signature)
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);

                loginResult.Success = true;
                loginResult.Token = token;
            }
            else
            {
                loginResult.Success = false;
                loginResult.Message = "UserName or Password is incorrect";
            }
            return loginResult;
        }
        public async Task<MemberRegisteration> GetUserProfile(string userId)
        {

            var userDetails = new MemberRegisteration();
            var user = await _userManager.FindByIdAsync(userId);
            var role = await _userManager.GetRolesAsync(user);
            userDetails.Birthdate = user.BirthDay;
            userDetails.PhoneNumber = user.PhoneNumber;
            userDetails.Firstname = user.FirstName;
            userDetails.Lastname = user.lastName;
            userDetails.Email = user.Email;
            userDetails.Role = role.FirstOrDefault();
            userDetails.UserId = user.Id;
            return userDetails;

        }

        public async Task<OperationResult<string>> ConfirmEmail(string userId, string code)
        {
            OperationResult<string> result = new OperationResult<string>();
            ApplicationUser appUser = _userManager.Users.FirstOrDefault(x => x.Id == userId);
            IdentityOptions options = new IdentityOptions();
            if (userId == null || code==null)
            {
                result.Success = false;
                result.Message = "There is a problem. We are not able to activate your account, Please contact your administrator. ";
            }
            var provider = options.Tokens.EmailConfirmationTokenProvider;

            bool isValid = await _userManager.VerifyUserTokenAsync(appUser, provider, "EmailConfirmation", HttpUtility.UrlDecode(code));

            if (isValid)
            {
                var data = await _userManager.ConfirmEmailAsync(appUser, HttpUtility.UrlDecode(code));
                if (data.Succeeded)
                {
                    _logger.LogInformation("user registered for email");
                    result.Success = true;
                    result.Message = "Your account has been activated successfully.";
                }
                else
                {
                    _logger.LogInformation("second message for email conf");
                    result.Success = false;
                    result.Message = "Your account confirmation link has been expired or invalid link,Please contact your administrator.";
                }

            }
            else
            {
                _logger.LogInformation("third error message for email conf not valid");
                result.Success = false;
                result.Message = "Your account confirmation link has been expired or invalid link,Please contact your administrator.";
            }
            return result;

        }

        public async Task<OperationResult<string>> SaveUserInformation(MemberRegisteration model)
        {
            OperationResult<string> result = new OperationResult<string>();
            var user = await _userManager.FindByIdAsync(model.UserId);
            user.FirstName = model.Firstname;
            user.lastName = model.Lastname;
            user.LastUpdated = DateTime.UtcNow;
            user.LastUpdateBy = model.Firstname;
            var userResult = await _userManager.UpdateAsync(user);

            if (userResult.Succeeded)
            {
                result.Success = true;
                result.Message = "Profile has been updated Successfully.";
            }
            return result;
        }

        public async Task SignOut()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<OperationResult<string>> DeleteUser(string userId)
        {
            OperationResult<string> result = new OperationResult<string>();
            var user = await _userManager.FindByIdAsync(userId);
            var userDelete = await _userManager.DeleteAsync(user);
            if (userDelete.Succeeded)
            {
                result.Success = true;
                result.Message = "User Deleted Successfully";
            }
            
                
                else
                {
                    result.Message = "There is some problem. Not able to change your password. Please Contact your Administrator.";
                }
                
            return result;
        }

        public async Task<OperationResult<string>> ChangePassword(MemberRegisteration model)
        {
            OperationResult<string> result = new OperationResult<string>();
            var user = await _userManager.FindByIdAsync(model.UserId);
            var passwordResult = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.Password);

            if (passwordResult.Succeeded)
            {
                result.Success = true;
                result.Message = "Password has been changed successfully.";

            }
            else
            {
                string error = passwordResult.Errors.FirstOrDefault().Code;

                if (error == "PasswordMisMatch")
                {
                    result.Message = "Current Password is Incorrect.Please try again";
                    
                }
                else
                {
                    result.Message = "There is some problem. Not able to change your password. Please Contact your Administrator.";
                }
                result.Success = false;
            }
            return result;
        }

        

        public async Task<OperationResult<string>> ForgotPasswordEmail(string email)
        {
            OperationResult<string> result = new OperationResult<string>();
            var user = await _userManager.FindByNameAsync(email);
            if (user == null)
            {
                result.Message = "Not able to find entered email in system, Please try again";
                result.Success = false;
                return result;
            }

            var code = await _userManager.GeneratePasswordResetTokenAsync(user);

            ApplicationSettings appSettings = new ApplicationSettings();
            var url = appSettings.GetConfigurationValue("ApplicationUrl", "PasswordResetUrl");

            var callBackUrl = url + "?userId=" + user.Id + "&code=" + HttpUtility.UrlEncode(code);
            
            await _emailSender.SendEmails(email, user.FirstName, user.lastName, HtmlEncoder.Default.Encode(callBackUrl),"Password Reset Link","ForgotPassword");
            result.Success = true;
            return result;
        }

        public async Task<OperationResult<string>> ResetPassword(ResetPassword model)
        {
            OperationResult<string> result = new OperationResult<string>();

            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user !=null)
            {
                IdentityOptions options = new IdentityOptions();
                var provider = options.Tokens.EmailConfirmationTokenProvider;
                bool isValid = await _userManager.VerifyUserTokenAsync(user, provider, "ResetPassword", HttpUtility.UrlDecode(model.Code));

                if (isValid)
                {
                    var resetPassword = await _userManager.ResetPasswordAsync(user, HttpUtility.UrlDecode(model.Code), model.Password);

                    if (resetPassword.Succeeded)
                    {
                        result.Success = true;
                        result.Message = "Your Password has been Updated Successdully";
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "There is some problem. We are not able to reset your password, Please contact your Administrator";
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Your Password reset link has been expired, Please generate different link and try again.";
                }
            }
            else
            {
                result.Success = false;
                result.Message = "Your Password reset link has been expired, Please generate different link and try again.";
            }
            return result;
        }

        public  List<ShowTask> GenerateTasksToUser(/*MemberRegisteration model, Taskslist tasks*/ /*string userId*/)
        {



            List<ShowTask> task= _userDbContext.showTasks.ToList();

            /*var count = 0;
            if (task != null)
            {
                foreach (var t in task)
                {
                    t.FirstName = t.FirstName;
                    t.LastName = t.LastName;
                    t.PreviousTask = t.PreviousTask;
                    t.CurrentTask = t.CurrentTask;
                    if (t.CurrentTask == t.PreviousTask)
                    {
                        for (int i = 0; i < t.CurrentTask.Length; i++)
                        {
                            return task;

                        }
                    }

                }
            }*/

            return task;

           
        }

        public  List<KitchenTask> GetKitchenTasks()
        {
            int tid = 1;

            List<KitchenTask> tasks = _userDbContext.kitchenTasks.Where(x=> x.task_id.Equals(tid)).ToList();
            

            return tasks;
        }
        public List<Tuple<ApplicationUser,IdentityRole>> SendEmailToAdmin()
        {
            /*var users = _userDbContext.Users
.SelectMany(
    // -- below emulates a left outer join, as it returns DefaultIfEmpty in the collectionSelector
    user => _userDbContext.UserRoles.Where(userRoleMapEntry => user.Id == userRoleMapEntry.UserId).DefaultIfEmpty(),
    (user, roleMapEntry) => new { User = user, RoleMapEntry = roleMapEntry })
.SelectMany(
    // perform the same operation to convert role IDs from the role map entry to roles
    x => _userDbContext.Roles.Where(role => role.Id == x.RoleMapEntry.RoleId).DefaultIfEmpty(),
    (x, role) => new { User = x.User, Role = role })
.ToList() // runs the queries and sends us back into EF Core LINQ world
.Aggregate(
    new Dictionary<ApplicationUser, List<IdentityRole>>(), // seed
    (dict, data) => {
        // safely ensure the user entry is configured
        dict.TryAdd(data.User, new List<IdentityRole>());
        if (null != data.Role)
        {
            dict[data.User].Add(data.Role);
        }
        return dict;
    },
    x => x);*/

            /*return users.ToList();*/


            return null;
            

            


        }

       /* public List<ApplicationUser> getuserslist()
        {
            List<ApplicationUser> user = SendEmailToAdmin();

        }*/


        public async Task DoWork()
        {
            
            var user = (from ur in  _userDbContext.UserRoles
                        join us in _userDbContext.Users on ur.UserId equals us.Id
                        join r in _userDbContext.Roles on ur.RoleId equals r.Id
                        where r.Name == "Admin"
                        select new
                        {
                            us.FirstName,
                            us.lastName,
                            us.Email,
                            ur.RoleId

                        }).ToList();
            List<KitchenTask> task = GetKitchenTasks();
            if (task != null)
            {
                foreach (var t in task)
                {
                    var message = (t.FirstName,"{0}'s Todays Task is").ToString();
                    _logger.LogInformation("Sending Email");
                    await _emailSender.SendtaskEmail(t.Email, t.FirstName, t.LastName, "Please Finish up your todays duty", "SendTaskEmail");
                    foreach(var u in user)
                    {
                        if (u.Email!="testuser@test.com")
                        {
                            _logger.LogInformation("sending email to admins");
                            await _emailSender.SendTaskEmailToAdmin(u.Email, u.FirstName, t.FirstName, "email to admin", "SendTaskEmail", message);
                        }
                        
                    }
                }
            }
            
            
        }

        /*public Task<int> UpdateAsync(KitchenTaskDto model)
        {
            throw new NotImplementedException();
        }*/

        public List<KitchenTask> UpdateAsync(KitchenTask model)
        {

            List<KitchenTask> taskid = _userDbContext.kitchenTasks.Where(x => x.task_id.Equals(model.task_id)).ToList();
            if (taskid != null)
            {
                foreach (var tasks in taskid)
                {
                    tasks.task_id = model.task_id;
                    tasks.FirstName = model.FirstName;
                    tasks.LastName = model.LastName;
                    tasks.Email = model.Email;
                };

                 _userDbContext.SaveChanges();

                return taskid.ToList();
            }

           return null;

            
        }

        public List<WeeklyTask> UpdateWeeklyTask(WeeklyTask model)
        {
            List<WeeklyTask> result = _userDbContext.weeklyTasks.Where(x => x.Task_id == model.Task_id).ToList();

            if (result != null)
            {
                foreach (var Tasks in result)
                {
                    Tasks.Task_id = model.Task_id;
                    Tasks.Id = model.Id;
                    Tasks.FirstName = model.FirstName;
                    Tasks.LastName = model.LastName;
                    Tasks.Email = model.Email;
                    Tasks.Weekly_Task_Id = model.Weekly_Task_Id;
                }

                _userDbContext.SaveChanges();

                return result.ToList();

            }

            return null;
        }

        public List<home_User_Check> AddHomeUser(HomeUser model)
        {
            var homeuser = _userDbContext.homeusercheck.Where(x => x.Id == model.Id).ToList();
            if (homeuser!=null)
            {
                foreach (var hu in homeuser)
                {
                    hu.home_id = model.Home_id.ToString();
                }
                _userDbContext.SaveChanges();
            }

            return homeuser;

            

        }

        public List<HomeUser> UpdatehomeUsers(HomeUser model)
        {
            var result = _userDbContext.home_users.Where(x => x.Id == model.Id).ToList();
            if (result != null)
            {
                foreach (var hu in result)
                {
                    hu.home_name = model.home_name;
                    hu.Id = model.Id;
                    hu.Home_id = model.Home_id;
                    hu.FirstName = model.FirstName;
                    hu.LastName = model.LastName;
                }
                _userDbContext.SaveChanges();
            }

            return result.ToList();
        }

        public List<KitchenTask> PushUserList()
        {
            var user = _userDbContext.kitchenTasks.AsNoTracking().ToList();
            int counter = 0;
            
            if (user!=null)
            {
                foreach (var users in user)
                {
                    users.FirstName = users.FirstName;
                    users.LastName = users.LastName;
                    users.Email = users.Email;
                    users.Task_Description = null;
                    users.Id = null;
                    users.task_id = (users.task_id - 1);
                    if (users.task_id == 0)
                    {
                        users.FirstName = users.FirstName;
                        users.LastName = users.LastName;
                        users.Task_Description = null;
                        users.Id = null;
                        users.Email = users.Email;
                        users.task_id = user.Count;
                    }
                    counter++;

                    //_userDbContext.kitchenTasks.Where(x => x.task_id == users.task_id);
                    //_userDbContext.SaveChanges();
                    
                    
                }
                
                
            }

            return user;
        }
        public List<KitchenTask> PushTask() 
        {
            List<KitchenTask> user = PushUserList();

            var counts = user.Count;

            if (user != null)
            {
                var counter = 0;
                foreach (var Task in user)
                {
                        Task.task_id = Task.task_id;
                        counter++;

                        List<KitchenTask> userDetails = _userDbContext.kitchenTasks.Where(x => x.task_id == Task.task_id).ToList();
                        if (userDetails != null)
                        {
                            foreach (var users in userDetails)
                            {
                                users.task_id = Task.task_id;
                                users.FirstName = Task.FirstName;
                                users.LastName = Task.LastName;
                                users.Email = Task.Email;
                                users.Task_Description = Task.Task_Description;
                                users.Id = Task.Id;

                            }
                            
                        }
                        
                        
                        

                    
                    counter++;
                }
            }
            _userDbContext.SaveChanges();

            return null;
        }

        public List<WeeklyTask> PushweeklyUserList()
        {
            var user = _userDbContext.weeklyTasks.AsNoTracking().ToList();
            var weeklytasklist = _userDbContext.weeklyTaskDescriptions.AsNoTracking().ToList();
            int counter = 0;

            if (user != null)
            {
                foreach (var users in user)
                {
                    
                    users.FirstName = users.FirstName;
                    users.LastName = users.LastName;
                    users.Email = users.Email;
                    users.Weekly_Task_Id = (users.Weekly_Task_Id - 1);
                    users.Id = null;
                    users.Task_id = (users.Task_id - 1);
                    
                    if (users.Task_id == 0 && users.Weekly_Task_Id==0)
                    {
                        users.FirstName = users.FirstName;
                        users.LastName = users.LastName;
                        users.Weekly_Task_Id = user.Count;
                        users.Id = null;
                        users.Email = users.Email;
                        users.Task_id = user.Count;
                    }
                    counter++;

                    //_userDbContext.kitchenTasks.Where(x => x.task_id == users.task_id);
                    //_userDbContext.SaveChanges();


                }


            }

            return user;
        }

        public List<WeeklyTask> PushWeeklyTask()
        {
            List<WeeklyTask> user = PushweeklyUserList();

            var counts = user.Count;

            if (user != null)
            {
                var counter = 0;
                foreach (var Task in user)
                {
                    Task.Task_id = Task.Task_id;
                    counter++;

                    List<WeeklyTask> userDetails = _userDbContext.weeklyTasks.Where(x => x.Task_id == Task.Task_id).ToList();
                    if (userDetails != null)
                    {
                        foreach (var users in userDetails)
                        {
                            users.Task_id = Task.Task_id;
                            users.FirstName = Task.FirstName;
                            users.LastName = Task.LastName;
                            users.Email = Task.Email;
                            users.Weekly_Task_Id = Task.Weekly_Task_Id;
                            users.Id = Task.Id;

                        }

                    }





                    counter++;
                }
            }
            _userDbContext.SaveChanges();

            return null;
        }

        public List<ShowWeeklyTask> ShowWeeklyTaskUser()
        {
            var tasklist = new ShowWeeklyTask();
            var user = (from wt in _userDbContext.weeklyTasks
                        join wtd in _userDbContext.weeklyTaskDescriptions on wt.Weekly_Task_Id equals wtd.Weekly_Task_Id
                        select new
                        {
                            wt.FirstName,
                            wt.LastName,
                            wt.Email,
                            wt.Task_id,
                            wtd.Task_Description
                        }).ToList();
            if (user!=null)
            {
                foreach (var users in user)
                {
                    tasklist.FirstName = users.FirstName;
                    tasklist.LastName = users.LastName;
                    tasklist.Email = users.Email;
                    tasklist.Task_Description = users.Task_Description;
                }
                
            }

            return null;

            
            
        }
    }
}
