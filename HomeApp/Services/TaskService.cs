﻿using HomeApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeApp.Data;

namespace HomeApp.Services
{
    public class TaskService : ITaskService
    {
        private readonly UserDbContext _userDbContext;

        public TaskService(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }
       
        public  Task<KitchenTask> UpdateTaskList(int taskId,KitchenTask task)
        {
            var updatetasklist = _userDbContext.kitchenTasks.FirstOrDefault(x => x.task_id == taskId);

          
            if (updatetasklist != null)
            {
                _userDbContext.Entry<KitchenTask>(updatetasklist).CurrentValues.SetValues(task);
                _userDbContext.SaveChanges();
            }
            
            
                return null;
            
        }
    }
}
