﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeApp.Model;

namespace HomeApp.Services
{
    public interface ITaskService
    {
        Task<KitchenTask> UpdateTaskList(int taskId,KitchenTask task);
    }
}
