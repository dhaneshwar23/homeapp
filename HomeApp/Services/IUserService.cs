﻿using HomeApp.Model;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Services
{
    public interface IUserService
    {
        /*Task<List<MemberRegisteration>> GetUsersAsync();*/
        Task<IdentityResult> RegisterUserAsync(MemberRegisteration memberRegisteration);
        Task<IdentityResult> RegisterAdminAsync(MemberRegisteration memberRegisteration);

        Task<LoginResult> SignInAsync(MemberLogin model);
        Task<MemberRegisteration> GetUserProfile(string userId);
        Task<OperationResult<string>> SaveUserInformation(MemberRegisteration model);

        Task SignOut();
        Task<OperationResult<string>> ConfirmEmail(string userId, string code);
        Task<OperationResult<string>> DeleteUser(string userId);
        Task<OperationResult<string>> ForgotPasswordEmail(string email);

        Task<OperationResult<string>> ResetPassword(ResetPassword model);
        Task<OperationResult<string>> ChangePassword(MemberRegisteration model);

        List<ShowTask> GenerateTasksToUser(/*MemberRegisteration model, Taskslist tasks*/);

        List<KitchenTask> GetKitchenTasks();

        public Task DoWork();

        List<KitchenTask> UpdateAsync(KitchenTask model);

        List<WeeklyTask> UpdateWeeklyTask(WeeklyTask model);
        List<Tuple<ApplicationUser, IdentityRole>> SendEmailToAdmin();

        List<home_User_Check> AddHomeUser(HomeUser model);

        List<HomeUser> UpdatehomeUsers(HomeUser model);

        List<KitchenTask> PushUserList();

        List<KitchenTask> PushTask();

        List<WeeklyTask> PushweeklyUserList();

        List<WeeklyTask> PushWeeklyTask();

        List<ShowWeeklyTask> ShowWeeklyTaskUser();



    }
}
