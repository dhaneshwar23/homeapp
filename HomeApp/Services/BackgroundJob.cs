﻿
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HomeApp.Services
{
    public class BackgroundJob : BackgroundService
    {
        
        private readonly IServiceProvider _service;
        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        private readonly ILogger<BackgroundJob> _logger;
        /*private string Schedule => "10 * * * * *"0 5 17 * * *;*/ //Runs every 10 seconds
        /* "*seconds *minutes *hours *day *month *year" */
        private string Schedule => "0 5 17 * * *";
        //private string Schedule => "0 0 12 * * *";


        public BackgroundJob(IServiceProvider service, ILogger<BackgroundJob> logger)
        {
            _logger = logger;
            _service = service;
            _schedule = CrontabSchedule.Parse(Schedule, new CrontabSchedule.ParseOptions { IncludingSeconds = true });
            DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(/*"America/Toronto"*/"Eastern Standard Time"));
            _nextRun = _schedule.GetNextOccurrence(currentTime);
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted started Service running.");
            do
            {
                _logger.LogInformation("Timed Hosted Service running.");
                var now = DateTime.Now;
                TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(/*"America/Toronto"*/"Eastern Standard Time");
                DateTime easternTime = TimeZoneInfo.ConvertTime(now, easternZone);
                /* DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now,America/Toronto TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
                 var nextrun = _schedule.GetNextOccurrence(currentTime);*/
                var timerun = (_nextRun - easternTime).TotalMilliseconds;
                /*foreach (var tz in TimeZoneInfo.GetSystemTimeZones())
                {
                    _logger.LogInformation(tz.Id);
                    Console.WriteLine(tz.Id);
                }*/
                _logger.LogInformation(timerun.ToString());
                _logger.LogInformation(easternTime.ToString());
                if (easternTime > _nextRun)
                {
                    _logger.LogInformation("Service Started");
                    
                    await DoWork();
                    _nextRun = _schedule.GetNextOccurrence(easternTime);
                }
                await Task.Delay(TimeSpan.FromMilliseconds(timerun), stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);
        }
        private async Task DoWork()
        {
            using (var scope = _service.CreateScope())
            {
                _logger.LogInformation("Do Work Started");
                var scopedProcessingService = scope.ServiceProvider.GetRequiredService<IUserService>();
                await scopedProcessingService.DoWork();
            }
        }
    }
}
