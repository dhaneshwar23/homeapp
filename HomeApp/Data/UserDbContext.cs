﻿using HomeApp.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Data
{
    
    public class UserDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<MemberRegisteration> memberRegisterations { get; set; }

        public DbSet<MemberLogin> memberLogins { get; set; }
        
        public DbSet<Country> country_name { get; set; }

        public DbSet<Province> provinces { get; set; }

        public DbSet<HomeRegisterations> home_registeration { get; set; }

        public DbSet<HomeUser> home_users { get; set; }

        public DbSet<ShowHomeUsers> home_user_detail { get; set; }

        public DbSet<ChangePassword> changepasswd { get; set; }

        public DbSet<Taskslist> tasklist { get; set; }

        public DbSet<ApplicationUser> AppUsers { get; set; }

        public DbSet<ShowTask> showTasks { get; set; }

        public DbSet<KitchenTask> kitchenTasks { get; set; }

        public DbSet<WeeklyTask> weeklyTasks { get; set; }

        public DbSet<home_User_Check> homeusercheck { get; set; }

        public DbSet<ApplicationUserBackUp> applicationuserBackup { get; set; }

        public DbSet<WeeklyTaskDescription> weeklyTaskDescriptions { get; set; }

        public DbSet<ShowWeeklyTask> showWeeklyTasks { get; set; }

        /* public DbSet<UserRegisteration> userRegisterations { get; set; }*/

        public UserDbContext(DbContextOptions<UserDbContext> options)
            :base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<MemberLogin>().ToTable("memberlogin");
            /*builder.Entity<MemberRegisteration>().ToTable("AspNetUsers");*/
            /*builder.Entity<Country>().ToTable("md_countries");
            builder.Entity<Province>().ToTable("md_provinces");*/
            builder.Entity<HomeRegisterations>().ToTable("tbl_home");
            builder.Entity<HomeUser>().ToTable("tbl_home_user");
            builder.Entity<Taskslist>().ToTable("tbl_tasks");
            builder.Entity<ShowTask>().ToTable("tbl_usergeneratedtask");
            /*builder.Entity<ShowHomeUsers>().ToTable("dbo.md_get_userhome").HasNoKey();*/
            /*builder.Entity<ChangePassword>().ToTable("tbl_member");*/
            builder.Entity<KitchenTask>().ToTable("tbl_kitchentask");
            builder.Entity<WeeklyTask>().ToTable("tbl_weeklyTask");
            builder.Entity<home_User_Check>().ToTable("tbl_home_User_Check");
            builder.Entity<ApplicationUserBackUp>().ToTable("AspNetUsers_BAK");
            builder.Entity<WeeklyTaskDescription>().ToTable("WeeklyTask_Description");


            builder.Entity<IdentityRole>().HasData(
                new { Id = "1", Name = "Admin", NormalizedName = "Admin", RoleName = "Admin", Handle = "admin"/*, RoleIcon = "/uploads/roles/icons/default/role.png"*/, IsActive = true },
                new { Id = "2", Name = "User", NormalizedName = "User", RoleName = "user", Handle = "user"/*, RoleIcon = "/uploads/roles/icons/default/role.png"*/, IsActive = true }
            );
        }

    }

    
}
