﻿using HomeApp.Data;
using HomeApp.Model;
using HomeApp.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Controllers
{
    public class HomeUsers : Controller
    {
        private readonly UserDbContext _userDbContext;

        private readonly IUserService _userService;

        public HomeUsers(UserDbContext userDbContext,
            IUserService userService
            )
        {
            _userDbContext = userDbContext;
            _userService = userService;
        }

        [HttpGet("api/gethomeusers")]

        public IActionResult GetHomeUsers()
        {
            var homeuser = _userDbContext.home_users.AsQueryable();

            return Ok(homeuser);
        }

        [HttpPost("api/AddHomeUser")]

        public IActionResult AddHomeUser([FromBody] HomeUser userObj)
        {
            if (userObj== null)
            {
                return BadRequest();
            }

            else
            {

                _userDbContext.home_users.Add(userObj);
                _userDbContext.SaveChanges();
                _userService.AddHomeUser(userObj);

                return Ok(new
                {
                    Status = 1,
                    Message = "User Added to Home"

                });
            }


        }
        [HttpPost("api/UpdateHomeUser")]

        public List<HomeUser> UpdateHomeUser ([FromBody]HomeUser model)
        {
            var result = _userService.UpdatehomeUsers(model);

            return result;
        }
    }
}
