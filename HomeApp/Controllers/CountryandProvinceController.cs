﻿using HomeApp.Data;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Controllers
{
    public class CountryandProvinceController : ControllerBase
    {

        private readonly UserDbContext _userDbContext;

        public CountryandProvinceController(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        [HttpGet("api/country")]
      
        public IActionResult GetCountries()
        {
            string storedProc = "exec md_get_countries";
            var countries = _userDbContext.country_name.FromSqlRaw(storedProc);

            return Ok(countries);
        }


        [HttpGet("api/province")]

        public IActionResult GetProvinces(int? country_id)
        {
            string storeProc = "exec md_get_provinces {0}";
            
            var province = _userDbContext.provinces.FromSqlRaw(storeProc,country_id);

            return Ok(province);
        }
    }
}
