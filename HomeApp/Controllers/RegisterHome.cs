﻿using HomeApp.Data;
using HomeApp.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Controllers
{
    public class RegisterHome : Controller
    {
        private readonly UserDbContext _userDbContext;

        public RegisterHome(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        [HttpGet("api/getHome")]

        public IActionResult GetHome()
        {
            var homes = _userDbContext.home_registeration.AsQueryable();

            return Ok(homes);
        }

        [HttpPost("api/RegisterHome")]

        public  IActionResult RegisterHomes([FromBody] HomeRegisterations usrObj)
        {
            if (usrObj == null)
            {
                return BadRequest();
            }

            else
            {
                _userDbContext.home_registeration.Add(usrObj);
                _userDbContext.SaveChanges();

                return Ok(new
                {
                    /*Status =200,*/
                    Message ="Home Registered Successfully"

                });
            }
        }

        [HttpDelete("api/DeleteHome")]
        
        public IActionResult DeleteHome([FromBody] HomeRegisterations Home_id)
        {
            
            _userDbContext.home_registeration.Remove(Home_id);
            _userDbContext.SaveChanges();

            return Ok(new
            {
                Message = "Home Deleted"

            });

        }
    }
}
