﻿using EmailService;
using HomeApp.Data;
using HomeApp.Model;
using HomeApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HomeApp.Controllers
{

    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IEmailSender _emailSender;
        private readonly UserDbContext _userDbContext;
        private readonly IUserService _userService;
        public LoginController(UserDbContext userDbContext, IEmailSender emailSender, IUserService userService)
        {
            _userDbContext = userDbContext;
            _emailSender = emailSender;
            _userService = userService;
        }
        


        [HttpPost("api/Login")]

        public async Task<LoginResult> Login(MemberLogin model)
        {
            return await _userService.SignInAsync(model);

        }

        [Authorize]
        [HttpGet("api/GetUsers")]
        public async Task<MemberRegisteration> GetUsers()
        {

            string userId = User.Claims.FirstOrDefault(c => c.Type == "UserID").Value;
            return await _userService.GetUserProfile(userId);

        }

        [HttpPost("api/SaveUserInformation")]
        public async Task<OperationResult<string>> SaveUserInformation(MemberRegisteration model) 
        {
            var result = await _userService.SaveUserInformation(model);
            return result;
        }


        [HttpPost("api/changePassword")]

        public async Task<OperationResult<string>> ChangePassword(MemberRegisteration model)
        {
            string userId = User.Claims.First(c => c.Type == "UserId").Value;
            model.UserId = userId;
            var result = await _userService.ChangePassword(model);

            return result;
        }

        [HttpGet("api/SignOut")]

        public async Task SignOut()
        {
            await _userService.SignOut();
        }
    }
        
    }

