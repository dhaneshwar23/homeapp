﻿using HomeApp.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Controllers
{
    public class ShowusersHome : Controller
    {
        private readonly UserDbContext _userDbContext;

        public ShowusersHome(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        [HttpGet("api/homeusers")]

        public IActionResult getHomeUsers(string Id)
        {
            string storedProc = "exec get_homeusers {0}";
            var userhomes = _userDbContext.home_user_detail.FromSqlRaw(storedProc,Id).ToList();
            
            return Ok(userhomes);
            
            
        }
    }
}
