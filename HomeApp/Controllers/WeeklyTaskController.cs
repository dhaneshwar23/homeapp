﻿using HomeApp.Data;
using HomeApp.Model;
using HomeApp.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeApp.Controllers
{
    public class WeeklyTaskController:ControllerBase
    {
        private readonly UserDbContext _userDbContext;

        private readonly IUserService _userService;

        public WeeklyTaskController(UserDbContext userDbContext,
            IUserService userService
            )
        {
            _userDbContext = userDbContext;
            _userService = userService;
        }
        [HttpGet("api/GetWeeklyTaskList")]

        public IActionResult WeeklytaskList() 
        {
            var result = _userDbContext.weeklyTasks.AsQueryable();

            return Ok(result);
        }

        [HttpPost("api/AddweeklyTask")]

        public IActionResult AddWeeklyTask([FromBody] WeeklyTask model) 
        {

            if (model == null)
            {
                return BadRequest();
            }

            else
            {
                _userDbContext.weeklyTasks.Add(model);
                _userDbContext.SaveChanges();

                return Ok(new
                {
                    Message = "Home Added Successfully"

                });
            }
            

            
        }

        [HttpPost("api/UpdateWeeklyTaskList")]

        public List<WeeklyTask> UpdateWeeklyTaskList([FromBody] WeeklyTask model)
        {
            
            var result = _userService.UpdateWeeklyTask(model);

            return result;
        }
    }
}
