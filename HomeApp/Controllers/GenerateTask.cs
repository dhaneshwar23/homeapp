﻿using HomeApp.Data;
using HomeApp.Model;
using HomeApp.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HomeApp.Controllers
{
    public class GenerateTask : Controller
    {
        private readonly UserDbContext _userDbContext;
        private readonly IUserService _userService;
        private readonly ITaskService _taskService;

        public GenerateTask(UserDbContext userDbContext, IUserService userService, ITaskService taskService)
        {
            _userDbContext = userDbContext;
            _userService = userService;
            _taskService = taskService;
        }

        [HttpGet("api/GetWeeklyTask")]
        public List<ShowTask> /*IActionResult*/ GetTasksList()
        {

            var result = _userService.GenerateTasksToUser();

            return result;
        }

        [HttpPost("api/AddTaskToUser")]

        public IActionResult AddTask([FromBody] ShowTask model)
        {
            var result = _userDbContext.showTasks.Add(model);
            _userDbContext.SaveChanges();

            return Ok(result);
        }


        [HttpGet("api/getKitchenTask")]

        public List<KitchenTask> GetKitchenTask()
        {
            var result = _userDbContext.kitchenTasks.ToList();

            return result;
        }

        [HttpGet("api/CurrentTaskUser")]

        public List<KitchenTask> getTasks()
        {
            var result = _userService.GetKitchenTasks();

            return result;
        }

        

        public IActionResult UpdateTaskList(/*int task_id,string firstname,string lastname,string email,*/ KitchenTask model)
        {
            /*string storeProc = "exec UpdateTaskList @task_id,@firstname,@lastname,@email";

            var result = _userDbContext.kitchenTasks.FromSqlRaw(storeProc, task_id, firstname, lastname, email);

            return Ok(result);*/

            var updatetasklist = _userDbContext.kitchenTasks.FirstOrDefault(x => x.task_id == model.task_id);

            if (updatetasklist == null)
            {
                return BadRequest();
            }
            else
            {

                updatetasklist.task_id = model.task_id;
                updatetasklist.FirstName = model.FirstName;
                updatetasklist.LastName = model.LastName;
                updatetasklist.Email = model.Email;

                _userDbContext.SaveChanges();

                return Ok(new
                {
                    Message = "List Updated Successdully"

                });
            }

            /*if (model == null)
            {
                return BadRequest();
            }
            else
            {
                _userDbContext.kitchenTasks.Update(model);
                _userDbContext.SaveChanges();

                return Ok(new { 
                status = 200,
                message ="List Updated Succesfully."
                });
            }*/



        }

        [HttpPost("api/swapKitchenTask")]

        public List<KitchenTask> updatetasks([FromBody] KitchenTask model)
        {
            var result = _userService.UpdateAsync(model);
            return result;
        }
        [HttpGet("api/doneKitchenlist")]

        public List<KitchenTask> PushUserList()
        {
            var result = _userService.PushTask();

            return result;
        }
       [HttpGet("api/doneWeeklyTask")]

       public List<WeeklyTask> PushWeeklytaskList()
        {
            var result = _userService.PushWeeklyTask();

            return result;
        }

        [HttpGet("api/getWeeklyUserTask")]

        public List<ShowWeeklyTask> ShowWeeklyTasks()
        {
            var storeProc = "exec get_weeklyTask_User";

            var result = _userDbContext.showWeeklyTasks.FromSqlRaw(storeProc).ToList();

            return result.ToList();
        }
    }
}
