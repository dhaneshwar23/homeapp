﻿using Dapper;
using HomeApp.Configuration;
using HomeApp.Data;
using HomeApp.Model;
using HomeApp.Model.DTO_s.Responses;
using HomeApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HomeApp.Controllers
{
    
    [ApiController]
    public class RegisterController : Controller
    {
        private readonly UserDbContext _userDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserService _userService;
        /*private readonly UserManager<UserRegisteration> _userManager;*/
        private readonly JwtConfig _jwtConfig;

        public RegisterController(UserDbContext userDbContext, /*UserManager<UserRegisteration> userManager,*/ IOptionsMonitor<JwtConfig> optionsMonitor,IUserService userService,UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _userDbContext = userDbContext;
            _userService = userService;
            /*_userManager = userManager;*/
            _jwtConfig = optionsMonitor.CurrentValue;
        }

        [HttpPost("api/Register")]
        public async Task<IdentityResult> Register(MemberRegisteration model)
        {
            
            if (string.IsNullOrEmpty(model.Role))
            {
                model.Role = "User";
            }
            if (string.IsNullOrEmpty(model.CreatedBy))
            {
                model.CreatedBy = model.Firstname;
            }
            
            var result = await _userService.RegisterUserAsync(model);

            return result;
        }
        [HttpPost("api/Admin")]
        public async Task<IdentityResult> RegisterAdmin(MemberRegisteration model)
        {
            if (string.IsNullOrEmpty(model.Role))
            {
                model.Role = "Admin";
            }
            if (string.IsNullOrEmpty(model.CreatedBy))
            {
                model.CreatedBy = model.Email;
            }
            var result = await _userService.RegisterAdminAsync(model);

            return result;
        }
        [HttpGet,Route("api/ConfirmEmail")]
        public async Task<OperationResult<string>> ConfirmEmail(string userId,string code)
        {
            OperationResult<string> result = new OperationResult<string>();
            result = await _userService.ConfirmEmail(userId, code);
            return result;
        }
        [HttpGet("api/SendForgotPassword")]

        public async Task<OperationResult<string>> ForgotPassword(string email)
        {
            OperationResult<string> result = new OperationResult<string>();
            result = await _userService.ForgotPasswordEmail(email);
            return result;
        }

        [HttpPost("api/ResetPassword")]
        public async Task<OperationResult<string>> ResetPassword(ResetPassword model)
        {
            OperationResult<string> result = new OperationResult<string>();
            result = await _userService.ResetPassword(model);
            return result;
        }

        [HttpPost("api/getUseridforhome")]

        public IActionResult GetUserIdForHome(string Id)
        {
            var result = _userDbContext.homeusercheck.Where(x => x.Id == Id).ToList();

            return Ok(result);
        }

        [HttpDelete("api/DeleteUser")]

        public async Task<OperationResult<string>> DeleteUser(string Id)
        {
            var result = await _userService.DeleteUser(Id);

            return result;
        }



        private string GenerateJwtToken(IdentityUser user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]{

                new Claim("Id",user.Id),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(6),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtTokenHandler.WriteToken(token);

            return jwtToken;
        }
    }
}
